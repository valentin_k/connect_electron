var clipboard = require('clipboard');

const clipboardWatcher = require('electron-clipboard-watcher');

$(function(){

    var messages_on_screen=6;
    var pointer=0;
    var wanted_pointer=0;
    var messages=[];

    var we_touched_clipboard=false;

    var types={};

    types['message']=['body'];
    types['login']=['room_id'];
    types['sign_up']=[];
    types['technical']=['code','text'];
    types['id_respond']=['qr','room_id'];

    var code={};

    code[-1]='incoming message does not fit json format'
    code[-2]='wrong message format'
    code[-3]='you need to login first'
    code[-4]='wrong room id'
    code[-5]='unkown error'

    code[3]='login success'
    code[1]='message sent'

    var adress='ws://localhost:8765';
    var connection= new WebSocket(adress);

    //var clipboard = require('clipboard');

    function view_dots (direction){
      $('.messages').append('<div class="list-group-item dots" id="messages_'+direction+'" > <div class="list-group-item dots_text"> . . .</div> </div>');
    }

    function view_message (i){
      m=messages[i];

      $('.messages').append('<div class="list-group-item '+m['type']+'" id="message_'+i+'" > <div class="list-group-item">'+m['text']+'</div> </div>');
    }

    //ok, time for some indian code
    // how to insert elements in the middle?
    function render(){
      var i;  // let isnt working im my electron
      var l=messages.length;

      $( "#messages_begining" ).remove();
      for (i=0;i<Math.min(messages_on_screen, l) ;i++){
        $( "#message_"+(pointer+i) ).remove();
      }
      $( "#messages_end" ).remove();


      //
      if(wanted_pointer >= 0 && wanted_pointer+messages_on_screen <= l){
        pointer=wanted_pointer;
      }
      else {
        wanted_pointer=pointer;
      }


      if (pointer>0){
        view_dots('begining')
      }

      for (i=0; i<messages_on_screen && i<l; i++){
        view_message(i+pointer);
      }

      if (pointer+messages_on_screen<l){
        view_dots('end');
      }

    }

    function add_message (m){
      messages.push(m);
      $('.badge').text(''+messages.length);

      // in case we do not see new messages
      if(pointer+messages_on_screen < messages.length-1 )
      {
        $('#messages_end').css('background-color', 'darkgreen ');
      }
      else
      {
        wanted_pointer++;
        render();

      }
    }

    function send_message(text)
    {
      var m=JSON.stringify({type:'message','body':text});
      connection.send(m);

      add_message({'text':text,'type':'outgoing'})
    }


    connection.onmessage = function (event) {
    var m=JSON.parse(event.data)

      if (m['type']=='technical'){
          if (m['code']!=1){
            add_message({'text':m['text'],'type':m['type']});
          }
        }

      else if (m['type']=='message') {
          add_message({'text':m['body'],'type':'incoming'});
          we_touched_clipboard=true;
          clipboard.writeText(m['body']);
      }

      else if (m['type']=='id_respond') {
          //var qr_adress=m['qr']
          var qr_adress='/var/www/html/qr/'+m['room_id']+'.png';
          //change QR source in release

          $(".QR_image").attr("src",qr_adress);
          $('.id_box').val(m['room_id']);

          add_message({'text':'room created','type':'technical'});

      }
    }

    $(".btn-create").click(function(){
      var m=JSON.stringify({type:'sign_up'});

      connection.send(m);
    });


    $(".btn-connect").click(function(){

      var id=''+$('.id_box').val();
      var m=JSON.stringify({type:'login','room_id':id});

      connection.send(m);
    });

    $(".btn-send").click(function(){
      var text=''+$('.message_box').val();
      send_message(text);
      $('.message_box').val('');
    });


    $('.messages').on('mousewheel', function(event) {
      wanted_pointer-=event.deltaY;
      render();
    });

    clipboardWatcher({
      watchDelay: 250,
      onImageChange: function (nativeImage) {  },
      onTextChange: function (text) {
        if (we_touched_clipboard)
        {
            we_touched_clipboard=false;
        }
        else {
            send_message(text);
        }
      }
    });

});
